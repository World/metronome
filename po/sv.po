# Swedish translation for metronome.
# Copyright © 2021-2024 metronome's COPYRIGHT HOLDER
# This file is distributed under the same license as the metronome package.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2021, 2023, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: metronome master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/metronome/-/issues\n"
"POT-Creation-Date: 2024-01-06 20:40+0000\n"
"PO-Revision-Date: 2024-02-12 15:58+0100\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.4.2\n"

#: data/com.adrienplazas.Metronome.desktop.in.in:3
#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:7 src/main.rs:25
msgid "Metronome"
msgstr "Metronom"

#: data/com.adrienplazas.Metronome.desktop.in.in:4
#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:8 src/application.rs:120
msgid "Keep the tempo"
msgstr "Håll tempot"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.adrienplazas.Metronome.desktop.in.in:10
msgid "Bar;Beats;BPM;Measure;Minute;Practice;Rhythm;Tap;Tempo;Time;"
msgstr ""
"Takt;Bar;Beats;Slag;BPM;Mätning;Minut;Träning;Övning;Rytm;Klicka;Knacka;"
"Tempo;Tid;"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:10
msgid ""
"Metronome beats the rhythm for you, you simply need to tell it the required "
"time signature and beats per minute."
msgstr ""
"Metronom slår rytmen åt dig, du behöver bara ge det tidssignaturen som krävs "
"och antalet slag per minut."

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:11
msgid ""
"You can also tap to let the application guess the required beats per minute."
msgstr ""
"Du kan också klicka för att låta programmet gissa de slag per minut som "
"krävs."

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:16
msgid "Main window"
msgstr "Huvudfönster"

#: data/com.adrienplazas.Metronome.metainfo.xml.in.in:20
msgid "Main window, narrow and running"
msgstr "Huvudfönster, smalt och under körning"

#: data/com.adrienplazas.Metronome.gschema.xml.in:6
#: data/com.adrienplazas.Metronome.gschema.xml.in:7
msgid "Default window width"
msgstr "Standardbredd för fönster"

#: data/com.adrienplazas.Metronome.gschema.xml.in:11
#: data/com.adrienplazas.Metronome.gschema.xml.in:12
msgid "Default window height"
msgstr "Standardhöjd för fönster"

#: data/com.adrienplazas.Metronome.gschema.xml.in:16
msgid "Default window maximized behaviour"
msgstr "Standardmaximeringsbeteende för fönster"

#: data/com.adrienplazas.Metronome.gschema.xml.in:22
#: data/com.adrienplazas.Metronome.gschema.xml.in:23
msgid "Default beats per bar"
msgstr "Standardslag per takt"

#: data/com.adrienplazas.Metronome.gschema.xml.in:28
#: data/com.adrienplazas.Metronome.gschema.xml.in:29
msgid "Default beats per minute"
msgstr "Standardslag per minut"

#: data/resources/ui/shortcuts.blp:13
msgctxt "shortcut window"
msgid "General"
msgstr "Allmänt"

#: data/resources/ui/shortcuts.blp:16
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Visa kortkommandon"

#: data/resources/ui/shortcuts.blp:21
msgctxt "shortcut window"
msgid "Quit"
msgstr "Avsluta"

#: data/resources/ui/shortcuts.blp:26
msgctxt "shortcut window"
msgid "Tap"
msgstr "Klicka"

#: data/resources/ui/shortcuts.blp:31
msgctxt "shortcut window"
msgid "Play/Pause"
msgstr "Spela/Pausa"

#: data/resources/ui/timerbutton.blp:23
msgid "Play"
msgstr "Spela"

#: data/resources/ui/timerbutton.blp:38
msgid "Pause"
msgstr "Pausa"

#: data/resources/ui/window.blp:7
msgid "_Keyboard Shortcuts"
msgstr "_Tangentbordsgenvägar"

#: data/resources/ui/window.blp:12
msgid "_About Metronome"
msgstr "_Om Metronom"

#: data/resources/ui/window.blp:53
msgid "2/4 Time"
msgstr "2/4-takt"

#: data/resources/ui/window.blp:65
msgid ""
"2\n"
"4"
msgstr ""
"2\n"
"4"

#: data/resources/ui/window.blp:71
msgid "3/4 Time"
msgstr "3/4-takt"

#: data/resources/ui/window.blp:84
msgid ""
"3\n"
"4"
msgstr ""
"3\n"
"4"

#: data/resources/ui/window.blp:90
msgid "4/4 Time"
msgstr "4/4-takt"

#: data/resources/ui/window.blp:103
msgid ""
"4\n"
"4"
msgstr ""
"4\n"
"4"

#: data/resources/ui/window.blp:110
msgid "6/8 Time"
msgstr "6/8-takt"

#: data/resources/ui/window.blp:122
msgid ""
"6\n"
"8"
msgstr ""
"6\n"
"8"

#: data/resources/ui/window.blp:164
msgid "Increase BPM"
msgstr "Öka slag/minut"

#: data/resources/ui/window.blp:176
msgid "Decrease BPM"
msgstr "Minska slag/minut"

#: data/resources/ui/window.blp:213
msgid "TAP"
msgstr "KLICKA"

#: data/resources/ui/window.blp:230
msgid "Main Menu"
msgstr "Huvudmeny"

#: src/application.rs:122
msgid ""
"Metronome beats the rhythm for you, you simply need to tell it the required "
"time signature and beats per minutes.\n"
msgstr ""
"Metronom slår rytmen åt dig, du behöver bara ge det tidssignaturen som krävs "
"och antalet slag per minut.\n"

#: src/application.rs:124
msgid ""
"You can also tap to let the application guess the required beats per minute"
msgstr ""
"Du kan också klicka för att låta programmet gissa de slag per minut som krävs"

#. Translators: Please enter your credits here (format: "Name https://example.com" or "Name <email@example.com>", no quotes)
#: src/application.rs:144
msgid "translator-credits"
msgstr ""
"Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Skicka synpunkter på översättningen till <tp-sv@listor.tp-sv.se>"
